#

find . -name '*~1' -delete
top=$(cd ..; git rev-parse --show-toplevel)
etcdir=/data/repos/gitlab.com/radiantSHIFT/AdoLE/dev/etc

mkgal.sh
qmasset=$(ipfs add -r . -Q)

set -e
if [ -e $etcdir/adole-tmpl.cfg ]; then
sed -e "s/\\\${qmasset}/$qmasset/" $etcdir/adole-tmpl.cfg > adole-cfg.json
if diff $etcdir/adole-tmpl.cfg adole-cfg.json; then false; fi
cp -p adole-cfg.json $etcdir
fi

target=safewatch
rsync -auP adole-cfg.json $target:/var/www/demo/etc/adole-cfg.json
curl -s https://app.safewatch.care/etc/adole-cfg.json | grep -i Url
connect.sh dallas
curl -I https://ipfs.safewatch.care/ipfs/$qmasset | grep -i -e ipfs -e etag

git user
git commit -a -m "publishing assets on $(date +%m/%d)"

